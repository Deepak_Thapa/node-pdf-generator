const express = require("express");
const app = express();
const path = require("path");
const PORT = 5005
const pdfGenerate = require("./pdf-generator")

app.use(express.static(path.join(__dirname, "public")))
app.use(express.json())

app.post("/pdf-generate", (req, res) => {
    // console.log(req.body.html);
    const filename = "pdf-" + new Date().getTime() + (Math.random(10) * 100000).toFixed() + ".pdf"
    pdfGenerate(req.body.html, filename, res)
})

app.listen(PORT, () => {
    console.log("Server running...")
})