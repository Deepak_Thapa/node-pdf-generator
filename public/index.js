const submit_btn = document.getElementById("submit-btn");

submit_btn.addEventListener("click", () => {
    submitPdf()
})

const submitPdf = () => {
    const html_input = document.getElementById("html-input");
    if (html_input.value.length > 0) {
        const loading = document.getElementById("loading");
        loading.classList.add("loading-active");
        fetch("http://localhost:5005/pdf-generate", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                html: html_input.value
            })
        }).then(res => {
            if (res.status === 200) {
                loading.classList.remove("loading-active");
                loading.classList.add("loading-inactive");
                html_input.value = ""
                return res.blob()
            }
        })
            .then(resData => {
                let link = document.createElement('a');
                const link_container = document.getElementById("link-container");
                let objectURL = window.URL.createObjectURL(resData);
                link.href = objectURL;
                link.name = "link";
                link.target = '_self';
                link.download = "fileName.pdf";
                link_container.appendChild(link);
                link.click();
                setTimeout(() => {
                    window.URL.revokeObjectURL(objectURL);
                    link.remove();
                }, 100);

            })
            .catch(err => console.log(err))
    } else {
        alert("Cannot br empty")
    }
}