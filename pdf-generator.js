var fs = require('fs');
var pdf = require('html-pdf');
var options = {
    format: 'A4',
};

const pdfGenerate = (html, filename, response) => {
    pdf.create(html, options).toFile(`./${filename}`, (err, res) => {
        if (err) return console.log(err);
        try {
            console.log(filename)
            const data = fs.readFileSync(`./${filename}`);
            response.contentType("application/pdf");
            response.send(data);
        } catch (err) {
            response.send({ error: 1 })
        }
    });
}

module.exports = pdfGenerate